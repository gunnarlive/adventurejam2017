﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueBox : MonoBehaviour {

    public string message = "";
    public float time = .5f;

    private int characterIndex = 0;
    private Text comp;
    
    private void Awake()
    {
        comp = GetComponent<Text>();
    }

    private void Start()
    {
        DisplayMessage(message);
    }

    public void DisplayMessage(string msg)
    {
        comp.text = "";
        characterIndex = 0;
        message = msg;
        StartCoroutine(NextChar());
    }

    IEnumerator NextChar()
    {

        comp.text += message.ToCharArray()[characterIndex++];
        if (characterIndex < message.ToCharArray().Length)
        {
            yield return new WaitForSeconds(time);
            StartCoroutine(NextChar());
        }
    }

}
