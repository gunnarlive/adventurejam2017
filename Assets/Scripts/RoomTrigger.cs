﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomTrigger : MonoBehaviour {

	[SerializeField]
	public Transform position;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            PlayerController player = collision.gameObject.GetComponent<PlayerController>();
            player.transform.position = GetLocation();

            DialogueBox box = GameObject.FindGameObjectWithTag("dialogue").GetComponent<DialogueBox>();
            box.DisplayMessage("Fighting is only the begining.");
        }
    }

    private Vector3 GetLocation() {
		Camera.main.transform.position = new Vector3(position.position.x, position.position.y, -10);
		return position.position;
	}

}
