

		Things to do first:
			1: Art/Concept
			2: Player Controller
			3: Text Dialogue System


		Starting out:
			You are a miner looking for a new resource you
			heard stories about. You travel into this long
			forgotten mine.

		Battling system:
			Using your pick axe to throw and or melee with
			Energy levels depends what you do with your 
			pick axe and you can gain energy over time with
			out fighting. You can use your fists and it would
			not affect much of your energy level.

		End Goal:
			To find the mineral and destroy it after
			you realize that it is dangerous.