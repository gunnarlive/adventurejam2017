﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [SerializeField]
    private float playerSpeed = 2f;
    private Rigidbody2D body;
    private PlayerAnimator animator;

	// Use this for initialization
	void Awake() {
        body = GetComponentInChildren<Rigidbody2D>();
        animator = GetComponent<PlayerAnimator>();
    }

    // Update is called once per frame
    void Update()
    {
        float xmove = Input.GetAxisRaw("Horizontal");
        float ymove = Input.GetAxisRaw("Vertical");
        body.MovePosition(body.position + new Vector2(xmove / 256 * playerSpeed, ymove / 256 * playerSpeed));
        if (xmove + ymove != 0 || xmove + ymove != 0 || ymove != 0 || xmove != 0)
        {
            if (xmove < 0)
                animator.flipHorz = true;
            else
                animator.flipHorz = false;
            animator.isWalking = true;
        }
        else
        {
            animator.isWalking = false;
        }
    }

}


