﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour {

    private Animator animator;

    public bool isWalking;
    public bool flipHorz = false;

    Quaternion flip;
    Quaternion notFlip;

    SpriteRenderer[] renderers;

    private void Awake()
    {
        animator = GetComponentInChildren<Animator>();
        flip = new Quaternion(0, 180, 0, 0);
        notFlip = new Quaternion(0, 0, 0, 0);
    }

    private void Update()
    {

        if (flipHorz)
        {
            transform.rotation = flip;
        } else
        {
            transform.rotation = notFlip;
        }
        if (isWalking)
        {
            animator.SetBool("walking", true);
        } else
        {
            animator.SetBool("walking", false);
        }
    }

}
